// Telegram Configuration
process.env.NTBA_FIX_319 = 1;
require('dotenv').config();
const TelegramBot = require('node-telegram-bot-api');
const { TOKEN_TELEGRAM, PORT_SERVICE } = process.env;
const bot = new TelegramBot(TOKEN_TELEGRAM, { polling: true });

// Express Configuration
const express = require('express');
const http = require('http');
const app = express();
const server = http.createServer(app);
const options = {
    cors: true,
    origins: ['http://127.0.0.1:80'],
};
const io = require('socket.io')(server, options);

io.on('connection', function (socket) {
    bot.on('message', (msg) => {
        console.log({ msg });
        socket.emit('telegram-notification', msg.text ? msg.text : 'Urgency');
    });

    bot.on('polling_error', (err) => {
        socket.emit('telegram-notification', err);
        console.log(err);
    });
});

server.listen(PORT_SERVICE, function () {
    console.log('App running on *:', PORT_SERVICE);
});
